%isf semestralka
clear all;
a1 = -0.5;
b1 = 0.9 ;
c1 = -0.4;
lambda = 1^0.5 ; %normrnd bere odmocninu !
mereni = 500; % pocet realizaci

%generace procesu
y_S1=zeros(mereni,3);
y_S2=zeros(mereni,3);
u=zeros(mereni,3);
u(1,1)=1; % impuls
u(:,2)=ones(mereni,1); % skok
u(:,3)=wgn(mereni,1,0); % sum
e =normrnd(0,lambda,mereni,1);
for i =1:3 %pro vsechny mereni
    for t=2:mereni
        y_S1(t,i)=-a1*y_S1(t-1,i)+b1*u(t-1,i)+e(t);
        y_S2(t,i)=y_S1(t,i)+c1*e(t-1);
    end
end

% figure
% hold on
% subplot(3,2,1)
% plot(u(:,1))
% xlim([0 mereni])
% xlabel('k');
% ylabel('y');
% title('Vstupy');
% subplot(3,2,3)
% plot(u(:,2))
% xlim([0 mereni])
% xlabel('k');
% ylabel('y');
% subplot(3,2,5)
% plot(u(:,3))
% xlim([0 mereni])
% xlabel('k');
% ylabel('y');
% subplot(3,2,2)
% plot(y_S1(:,1))
% xlim([0 mereni])
% xlabel('k');
% ylabel('y');
% title('V�stupy');
% subplot(3,2,4)
% plot(y_S1(:,2))
% xlim([0 mereni])
% xlabel('k');
% ylabel('y');
% subplot(3,2,6)
% plot(y_S1(:,3))
% xlim([0 mereni])
% xlabel('k');
% ylabel('y');



%% LSM
A_S1=zeros(2,2,3);
A_S2=zeros(2,2,3);
b_S1=zeros(2,3);
b_S2=zeros(2,3);
odhad_S1=zeros(2,3);
odhad_S2=zeros(2,3);
for i =1:3
    A_S1(1,1,i)=y_S1(1:(end-1),i)'*y_S1(1:(end-1),i);
    A_S1(1,2,i)=-y_S1(1:(end-1),i)'*u(1:(end-1),i);
    A_S1(2,1,i)=A_S1(1,2,i);
    A_S1(2,2,i)=u(1:(end-1),i)'*u(1:(end-1),i);
    
    A_S2(1,1,i)=y_S2(1:(end-1),i)'*y_S2(1:(end-1),i);
    A_S2(1,2,i)=-y_S2(1:(end-1),i)'*u(1:(end-1),i);
    A_S2(2,1,i)=A_S2(1,2,i);
    A_S2(2,2,i)=u(1:(end-1),i)'*u(1:(end-1),i);
    
    b_S1(1,i)=-y_S1(1:(end-1),i)'*y_S1(2:end,i);
    b_S1(2,i)=u(1:(end-1),i)'*y_S1(2:end,i);
    
    b_S2(1,i)=-y_S2(1:(end-1),i)'*y_S2(2:end,i);
    b_S2(2,i)=u(1:(end-1),i)'*y_S2(2:end,i);

    odhad_S1(:,i)=A_S1(:,:,i)\b_S1(:,i);
    odhad_S2(:,i)=A_S2(:,:,i)\b_S2(:,i);
end
% 
%zpetnovazebni smycka 1
y_S1_Z=zeros(mereni,1);
k=0.4; % ?a1?k*b1 = 0.5?0.9*k unitcircle
for t=2:mereni
    y_S1_Z(t)=(-a1-k*b1)*y_S1_Z(t-1)+e(t);
end

% LSM
pom_u=y_S1_Z.*(-k);
A_S1_Z(1,1)=y_S1_Z(1:(end-1))'*y_S1_Z(1:(end-1));
A_S1_Z(1,2)=-y_S1_Z(1:(end-1))'*pom_u(1:( end-1));
A_S1_Z(2,1)=A_S1_Z(1,2);
A_S1_Z(2,2)=pom_u(1:(end-1))'*pom_u(1:(end-1));
b_S1_Z=[-y_S1_Z(1:(end-1))'*y_S1_Z(2:end);pom_u(1:(end-1))'*y_S1_Z(2:end)];

odhad_S1_zpetna_vazba = A_S1_Z\b_S1_Z;
% 
% zpetnovazebni regulator 2
y_S1_A=zeros(mereni,1);
kappas = [0.001,0.01,0.05,0.1,0.5,1,2,5,10,15];
kappa = sqrt(kappas(9));  
realizace = 400;
for m=1:realizace
    
v=normrnd(0,kappa,mereni,1);
for t=2:mereni
    y_S1_A(t)=(-a1-k*b1)*y_S1_A(t-1)+b1*v(t-1)+e(t);
end

% LSM
pom_u=y_S1_A.*(-k)+v;
A_S1_A(1,1)=y_S1_A(1:(end-1))'*y_S1_A(1:(end-1));
A_S1_A(1,2)=-y_S1_A(1:(end-1))'*pom_u(1:(end-1));
A_S1_A(2,1)=A_S1_A(1,2);
A_S1_A(2,2)=pom_u(1:(end-1))'*pom_u(1:(end-1));
b_S1_A=[-y_S1_A(1:(end-1))'*y_S1_A(2:end); pom_u(1:(end-1))'*y_S1_A(2:end)];
odhad_S1_alternativni_regulator(:,m) = A_S1_A\b_S1_A;
end

%teoreticky vypocet parametru
%S1
c1=0;
a_sum = a1 + (-c1*lambda*lambda*(1-a1*a1))/(b1*b1*kappa*kappa + (1+c1*c1-2*a1*c1)*lambda*lambda);
b_sum = b1;
a_imp = (a1*a1*c1 -a1*c1*c1 -a1 + c1)/(2*a1*c1-c1*c1 -1);
b_imp = (y_S1(1,1)*(c1-a1*c1*c1-a1+a1*a1*c1)-y_S1(2,1)*(1+c1*c1-2*a1*c1))/(2*a1*c1-c1*c1-1);
a_step = a1 - (c1*(1-a1*a1))/(1+c1*c1-2*a1*c1);
b_step = b1 - (b1*c1*(1-a1))/(1+c1*c1-2*a1*c1);
%S2

% c1=-0.4;
% 
% a_sum = a1 + (-c1*lambda*lambda*(1-a1*a1))/(b1*b1*kappa*kappa + (1+c1*c1-2*a1*c1)*lambda*lambda);
% b_sum = b1;
% a_imp = (a1*a1*c1 -a1*c1*c1 -a1 + c1)/(2*a1*c1-c1*c1 -1);
% b_imp= (y_S1(1,1)*(c1-a1*c1*c1-a1+a1*a1*c1)-y_S1(2,1)*(1+c1*c1-2*a1*c1))/(2*a1*c1-c1*c1-1);
% a_step = a1 - (c1*(1-a1*a1))/(1+c1*c1-2*a1*c1) ; 
% b_step = b1 - (b1*c1*(1-a1))/(1+c1*c1-2*a1*c1);  
%v�po�et variance
var_a_teo = (1/mereni)*((k^2 * lambda^2)/(kappa^2) + (lambda^2*(1-(a1+b1*k)^2))/(b1 * kappa^2+lambda^2))
var_b_teo = (1/mereni)*((lambda^2)/(kappa^2))
% var_real = lambda*inv((A_S1_A'*A_S1_A))
var_a_real = var(odhad_S1_alternativni_regulator(1,:))
var_b_real = var(odhad_S1_alternativni_regulator(2,:))

%Vysledky variance

var_a_teo_stat = [];
var_b_teo_stat = [];


