%ISF - 2.semestralni prace
clear all;
close all;
clc;
load isf_2_data.mat
X=X2;
Y=Y2;
Xide = X2(1:2:101)';
Xval = X2(2:2:100)';
Yide = Y2(1:2:101)';
Yval = Y2(2:2:100)';
delka= length(Xide);
%e = normrnd(0,1,delka,1);
order = 25; % max rad systemu
for i=1:length(X)
    if(mod(i,2) == 0) %sude
        Xide(ceil(i/2)) = X(i);
        Yide(ceil(i/2)) = Y(i);
    else
        Xval(ceil(i/2)) = X(i);
        Yval(ceil(i/2)) = Y(i);
    end
end
e_a = zeros(1,order);
e_b = zeros(1,order);
e_c = zeros(1,order);
e_a_val = zeros(1,order);
e_b_val = zeros(1,order);
e_c_val = zeros(1,order);
A = ones(delka,1);
y = Yide;
a = cell(1,order);
b = cell(1,order);
c = cell(1,order);
for i = 1:order
    A = [A,(Xide.^i)];
    %vypocet mnc
    %A) 
    a{i} = pinv(A)*y;
    %B)
    b{i} = inv(A'*A)*A'*y;
    %C)
    c{i} = flipud(polyfit(Xide,y,i)');
  
    %vypocet stredni kvadraticky chyby
    e_a(i) = sum(0.5*(A * a{i} - Yide).^2);
    e_b(i) = sum(0.5*(A * b{i} - Yide).^2);
    e_c(i) = sum(0.5*(A * c{i} - Yide).^2);
    
    
    
end   

%validacni cast
Aval = ones(delka,1);
for i = 1:order
    Aval = [Aval,(Xval.^i)];
     %vypocet stredni kvadraticky chyby
    e_a_val(i) = sum(0.5*(Aval * a{i} - Yval).^2);
    e_b_val(i) = sum(0.5*(Aval * b{i} - Yval).^2);
    e_c_val(i) = sum(0.5*(Aval * c{i} - Yval).^2);
    
    %show best polyfit
    if(i == 21)
        figure
        hold on
        grid on
        title('Aproxiamce polyfitem s nejmen�� chybou (��d 21)');
        plot(1:delka,Aval * c{i});
        plot(1:delka,Yval);
        legend('Odhad veliciny', 'Validace');
        xlabel('k');
        ylabel('y(k)');
    end
    %show best A&B
    if(i == 7)
        figure
        hold on
        grid on
        title('Aproxiamce metodou A s nejmen�� chybou (��d 7)');
        plot(1:delka,Aval * a{i});
        plot(1:delka,Yval);
        legend('Odhad veliciny', 'Validace');
        xlabel('k');
        ylabel('y(k)');
        
         figure
        hold on
        grid on
        title('Aproxiamce metodou B s nejmen�� chybou (��d 7)');
        plot(1:delka,Aval * b{i});
        plot(1:delka,Yval);
        legend('Odhad veliciny', 'Validace');
        xlabel('k');
        ylabel('y(k)');
    end
end

figure
grid on
hold on
title('A');
plot(1:order,e_a,'x-');
plot(1:order,e_a_val,'o-');
xlabel('rad systemu');
ylabel('kriterium chyby J');
[min_a,index_min_a] = min(e_a_val)
legend('identifikacni data chyba','validacni data chyba');
%plot(index_min_a,e_a(index_min_a),'ko');


figure
grid on
hold on
title('B');
plot(1:order,e_b,'x-');
plot(1:order,e_b_val,'o-');
xlabel('rad systemu');
ylabel('kriterium chyby J');
[min_b,index_min_b] = min(e_b_val)
legend('identifikacni data chyba','validacni data chyba');
%plot(index_min_b,e_b(index_min_b),'ko');
% 
% figure
% grid on
% hold on
% title('C');
% plot(1:order,e_c,'x-');
% plot(1:order,e_c_val,'o-');
% [min_c,index_min_c] = min(e_c)
%xlabel('rad systemu');
% ylabel('kriterium chyby J');
% legend('identifikacni data chyba','validacni data chyba');
% %plot(index_min_c,e_c(index_min_c),'ko');



       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       