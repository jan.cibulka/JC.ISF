%isf 2. semestralka
clear all;
close all;
clc;
a = 0.6;
b = 1.0 ;
c = 0.5;
lambda = 1;
lambda_vstup = 1;
delka = 1000000; % pocet realizaci

%generace procesu
e = normrnd(0,lambda,delka,1);
u = normrnd(0,lambda_vstup,delka,1);
y = zeros(delka,1);
ksi = zeros(delka,2);
phi = zeros(delka,2);

%metoda pridavne promenne
y(2)=a*y(2-1) + b*u(2-1) + e(2) + c*e(2-1);
ksi(2,:) =[u(2-1),0]';
phi(2,:) = [-y(2-1),u(2-1)]';
for t=3:delka
    y(t)=a*y(t-1) + b*u(t-1) + e(t) + c*e(t-1);
    ksi(t,:) = [u(t-1),u(t-2)]';
    phi(t,:) = [y(t-1),u(t-1)]';
end
 
% pom1 = [0 0 ; 0 0];
% pom2 = [0;0];
% for k =2:delka
% pom1 = pom1 + ksi(k,:)'*phi(k,:);
% pom2 = pom2 + ksi(k,:)'*y(k);
% end
% theta = inv(pom1)*pom2


%metoda chyby predikce
theta = [0.5 0.5 0.5];
eps=0.01;
i=0;
while(true)
    i=i+1;
    %urceni filtrovanych velicin v danem kroce
    psi = zeros(delka,3);
    
    %urceni filtrovaneho zaznamu
    chyba = zeros(1,delka);
    y_f = 0;
    u_f = 0;
    e_f = 0;
    pom1 = zeros(3,3);
    pom2 = zeros(3,1);
    
    for k=2:delka
        chyba(k) = y(k) - theta(1)* y(k-1) - theta(2)*u(k-1) - theta(3)*chyba(k-1);
        y_f = -y(k-1) - theta(3)*y_f;
        u_f = -u(k-1) - theta(3)*u_f;
        e_f = -chyba(k-1) - theta(3)*e_f;
        psi(k,:) = -[y_f,u_f,e_f];
    
        pom1 = pom1 + psi(k,:)'*psi(k,:);
        pom2 = pom2 + chyba(k)*psi(k,:)';
    end
    
    
    theta = theta + (inv(pom1)*pom2)';
    
    
    if(isnan(theta(1)))
        disp('chyba');
        break;
    end 
                          
    if( (inv(pom1)*pom2)'< [eps,eps,eps])
      i
      break;
    end
end
theta






% 
% %rozsirena metoda nejmen��ch �tverc�
% eps = 0;
% theta = [0; 0 ;0];
% P = [ 1 0 0;0 1 0 ;0 0 1 ];
% clear phi;
% log_theta = [0;0;0];
% log_p11 = [1];
% log_p22 = [1];
% log_p33 = [1];
% for k=2:delka
%     X = [y(k-1), u(k-1), eps];
%     eps = y(k) - X*theta;
%     L = (P*X')/(1+X*P*X');
%     theta = theta + L*eps;
%     P = P - L*X*P ;
%     
%     %logovaci cast
%     log_theta = [log_theta, theta];
%     log_p11 = [log_p11 P(1,1)];
%     log_p22 = [log_p22 P(2,2)];
%     log_p33 = [log_p33 P(3,3)];
% end
% figure(1)
% hold on
% grid on
% plot(1:delka, log_theta(1,:),'r-')
% plot(1:delka, a*ones(1,delka),'k-');
% plot(1:delka, log_theta(2,:),'g-')
% plot(1:delka, b*ones(1,delka),'k-');
% plot(1:delka, log_theta(3,:),'b-')
% plot(1:delka, c*ones(1,delka),'k-');
% legend('odhad a','a','odhad b','b','odhad c','c')
% title('rekurzivni metoda nejmen��ch �tverc�');
% 
% figure(2)
% hold on
% grid on
% plot(1:delka, log_p11,'r-');
% plot(1:delka, log_p22,'g-');
% plot(1:delka, log_p33,'b-');
% legend('p11','p22','p33');
% title('rekurzivni metoda nejmen��ch �tverc�');
% 
% close all;

% %rekurzivni metoda chyby predikce 
%  y_f = 0;
%  u_f = 0;
%  e_f = 0;
%  log_theta = [0;0;0];
% log_p11 = [1];
% log_p22 = [1];
% log_p33 = [1];
%  chyba = [0];
%  theta = [0; 0; 0];
%  P = [ 1 0 0;0 1 0 ;0 0 1 ];
%  for k=2:delka
%     chyba(k) = y(k) - theta(1)* y(k-1) - theta(2)*u(k-1) - theta(3)*chyba(k-1);
%     y_f = -y(k-1) - theta(3)*y_f;
%     u_f = -u(k-1) - theta(3)*u_f;
%     e_f = -chyba(k-1) - theta(3)*e_f;
%     psi = -[y_f,u_f,e_f]';
%     L = (P*psi)/(1+psi'*P*psi);
%     theta = theta + L*chyba(k);
%     P = P - (P*(psi*psi')*P)/(1+psi'*P*psi);
%     
%     %logovaci cast
%     log_theta = [log_theta, theta];
%     log_p11 = [log_p11 P(1,1)];
%     log_p22 = [log_p22 P(2,2)];
%     log_p33 = [log_p33 P(3,3)];
%  end
%  figure(1)
% hold on
% grid on
% plot(1:delka, log_theta(1,:),'r-')
% plot(1:delka, a*ones(1,delka),'k-');
% plot(1:delka, log_theta(2,:),'g-')
% plot(1:delka, b*ones(1,delka),'k-');
% plot(1:delka, log_theta(3,:),'b-')
% plot(1:delka, c*ones(1,delka),'k-');
% legend('odhad a','a','odhad b','b','odhad c','c')
% title('rekurzivni metoda chyby predikce');
% 
% figure(2)
% hold on
% grid on
% plot(1:delka, log_p11,'r-');
% plot(1:delka, log_p22,'g-');
% plot(1:delka, log_p33,'b-');
% legend('p11','p22','p33');
% title('rekurzivni metoda chyby predikce');


% %rekurzivni metoda pridavne promenne
% theta = [0;0];
% P = eye(2);
% log_theta = [0;0];
% log_p11 = [1];
% log_p22 = [1];
% for k=3:delka
%     ksi = [u(k-1) u(k-2)]';
%     phi  =[y(k-1) u(k-1)]';
%     e = y(k) - phi'*theta;
%     L =(P*ksi)/(1+phi'*P*ksi);
%     theta = theta + L*e;
%     P = P - L*phi'*P;
%     
% %   logovaci cast
%     log_theta = [log_theta, theta];
%     log_p11 = [log_p11 P(1,1)];
%     log_p22 = [log_p22 P(2,2)];
%     
% end
% 
% figure(1)
% hold on
% grid on
% plot(1:delka-1, log_theta(1,:),'r-')
% plot(1:delka-1, a*ones(1,delka-1),'k-');
% plot(1:delka-1, log_theta(2,:),'g-')
% plot(1:delka-1, b*ones(1,delka-1),'k-');
% legend('odhad a','a','odhad b','b')
% title('rekurzivni metoda pridavne promenne');
% 
% figure(2)
% hold on
% grid on
% plot(2:delka, log_p11,'r-');
% plot(2:delka, log_p22,'g-');
% legend('p11','p22');
% title('rekurzivni metoda pridavne promenne');









