function [xnew] = transformace_f(x)
dt= 0.02;
A = [1 0 dt 0 0 0;
      0 1 0 dt 0 0;
      0 0 1 0 1 0;
      0 0 0 1 0 1;
      0 0 0 0 1 0;
      0 0 0 0 0 1];
xnew = A * x;
end