function  [x_filt,P_filt] = rozsirenyKF()
%ISF 3. semestralni prace 2. cast - Rozsireny Filtr
clear all;
close all;
clc;

%Nacteni 
load isf_4_data.mat;

R=[4*10^(-4)*(pi/180)^2, 0;0,10^(-4)];
Q=10^(-6)*eye(4);
Q(5,5)=10^(-3);
Q(6,6)=10^(-3);

x_odh = [20,50,0,-12,0,0]';
P_odh = eye(6);
P_odh(6,6)=10^(-6);
P_odh(5,5)=10^(-6);

dt =0.02;
F=[1 0 dt 0 0 0;
   0 1 0 dt 0 0;
   0 0 1 0 1 0;
   0 0 0 1 0 1;
   0 0 0 0 1 0;
   0 0 0 0 0 1];


for i=1:455
    %FILTRACE
    z_odh(:,i)=[atan(x_odh(2,i)/x_odh(1,i));(x_odh(1,i)^2+x_odh(2,i)^2)^0.5];
    H=[-x_odh(2,i)/(x_odh(1,i)^2*(1+(x_odh(2,i)/x_odh(1,i))^2)) , 1/(x_odh(1,i)*(1+(x_odh(2,i)/x_odh(1,i))^2)) , 0,0,0,0;
        x_odh(1,i)/(x_odh(1,i)^2+x_odh(2,i)^2)^0.5,x_odh(2,i)/(x_odh(1,i)^2+x_odh(2,i)^2)^0.5,0,0,0,0];
    P_z = H*P_odh*H'+R;
    P_xz = P_odh*H';
    % Kalmanuv zisk
    K = P_xz*inv(P_z);
    % Filtracni stredni hodnota
    x_filt(:,i) = x_odh(:,i)+K*(z(:,i)-z_odh(:,i));
    % Kovariancni matice
    %P_filt(:,:,i) = P_odh - K*P_z*K';
    P_filt(:,:,i)=(eye(6)-K*H)*P_odh;
    
    %PREDIKCE
    % Prediktivni stredni hodnota
    x_odh(:,i+1) = F* x_filt(:,i) ;
    % Prediktivní kovariancni matice
    P_odh = F*P_filt(:,:,i)*F' + Q;
end
end









