close all;
clear all;
clc;
format long;
load isf_4_data.mat;
[ekf_xfilt,ekf_pfilt] = rozsirenyKF();
[ukf_xfilt,ukf_pfilt] = unscentovanyKF();

%rozsireny - cervene
%unscentovany - modre
%puvodni system / pomocne cary - cerne

%2D mapa
figure(1)
grid on
hold on
plot(ekf_xfilt(1,:),ekf_xfilt(2,:),'r');
plot(ukf_xfilt(1,:),ukf_xfilt(2,:),'b');
plot(x(1,:),x(2,:),'k');
legend('ekf','ukf','x');
title('2D mapa');
xlabel('x');
ylabel('y');
%start&cil
plot(ekf_xfilt(1,1),ekf_xfilt(2,1),'ro');
plot(ekf_xfilt(1,455),ekf_xfilt(2,455),'rx');
plot(ukf_xfilt(1,1),ukf_xfilt(2,1),'bo');
plot(ukf_xfilt(1,455),ukf_xfilt(2,455),'bx');

%pr�beh rychlost
figure(2)
grid on
hold on
plot(ekf_xfilt(3,:),ekf_xfilt(4,:),'r');
plot(x(3,:),x(4,:),'k');
legend('ekf','real');
title('Odhad rychlosti xy EKF');
xlabel('x');
ylabel('y');

%pr�beh rychlost
figure(3)
grid on
hold on
plot(ukf_xfilt(3,:),ukf_xfilt(4,:),'b');
plot(x(3,:),x(4,:),'k');
legend('ukf','real');
title('Odhad rychlosti xy UKF');
xlabel('x');
ylabel('y');

%3*sigma a chyba X
ukf_chyba_x = zeros(455,1);
ekf_chyba_x = zeros(455,1);
ukf_pxx = zeros(455,1);
ekf_pxx = zeros(455,1);
for i = 1:455
    ukf_chyba_x(i) = x(1,i) - ukf_xfilt(1,i);
    ekf_chyba_x(i) = x(1,i) - ekf_xfilt(1,i);
    ukf_pxx(i) = ukf_pfilt(1,1,i);
    ekf_pxx(i) = ekf_pfilt(1,1,i);
end
figure(4)
grid on;
hold on;
plot(ekf_chyba_x(:),'r');
plot(ukf_chyba_x(:),'b');
plot(3*sqrt(ekf_pxx(:)),'r--');
plot(3*sqrt(ukf_pxx(:)),'b--');
plot(-3*sqrt(ekf_pxx(:)),'r--');
plot(-3*sqrt(ukf_pxx(:)),'b--');
legend('ekf','ukf','ekf_ 3 sigma','ukf_ 3 sigma');
title('V�voj chyby odhadu a 3 sigma - X');
xlabel('krok k');
ylabel('velikost chyby');

%3*sigma a chyba Y
ukf_chyba_y = zeros(455,1);
ekf_chyba_y = zeros(455,1);
ukf_pyy = zeros(455,1);
ekf_pyy = zeros(455,1);
for i = 1:455
    ukf_chyba_y(i) = x(2,i) - ukf_xfilt(2,i);
    ekf_chyba_y(i) = x(2,i) - ekf_xfilt(2,i);
    ukf_pyy(i) = ukf_pfilt(2,2,i);
    ekf_pyy(i) = ekf_pfilt(2,2,i);
end
figure(5)
grid on;
hold on;
plot(ukf_chyba_y(:),'b');
plot(ekf_chyba_y(:),'r');
plot(3*sqrt(ukf_pyy(:)),'b--');
plot(3*sqrt(ekf_pyy(:)),'r--');
plot(-3*sqrt(ekf_pyy(:)),'r--');
plot(-3*sqrt(ukf_pyy(:)),'b--');
legend('ekf','ukf','ekf_ 3 sigma','ukf_ 3 sigma');
title('V�voj chyby odhadu a 3 sigma - Y');
xlabel('krok k');
ylabel('velikost chyby');

% euklidovska vzdalenost od skutecnosti UKF
ukf_eukl_chyba = zeros(455,1);
for i = 1:455
    ukf_eukl_chyba(i) = sqrt((x(2,i) - ukf_xfilt(2,i))^2+ (x(1,i) - ukf_xfilt(1,i))^2);
end
figure(6)
plot(ukf_eukl_chyba(:),'b');
legend('ukf','real');
title('Euklidovska chyba UKF');
xlabel('krok');
ylabel('velikost chyby');

% euklidovska vzdalenost od skutecnosti EKF
ekf_eukl_chyba = zeros(455,1);
for i = 1:455
    ekf_eukl_chyba(i) = sqrt((x(2,i) - ekf_xfilt(2,i))^2+ (x(1,i) - ekf_xfilt(1,i))^2);
end
figure(7)
plot(ekf_eukl_chyba(:),'r');
legend('ekf','real');
title('Euklidovska chyba EKF');
xlabel('krok');
ylabel('velikost chyby');

%rozdil eukleidovskych vzdalenosti
ukf_ekf_eukl_chyba_rozdil = zeros(455,1);
for i = 1:455
    ukf_ekf_eukl_chyba_rozdil(i) = ekf_eukl_chyba(i) - ukf_eukl_chyba(i);
end
figure(8)
plot(ukf_ekf_eukl_chyba_rozdil(:),'g');
legend('rozdil euklidovskych chyb ekf a ukf');
title('Rozdil euklidovskych chyb ekf a ukf');
xlabel('krok');
ylabel('velikost chyby');














