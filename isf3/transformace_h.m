function [z] = transformace_h(x)
z(1) = atan(x(2)/x(1));
z(2) = sqrt(x(2)^2 + x(1)^2);
end