function [x_filt,P_filt] = unscentovanyKF()
%ISF 3. semestralni prace 1. cast - UNSCENTOVANY KF
clear all;
close all;
clc;

%Nacteni 
load isf_4_data.mat;

%Unscentovany KF
R=[4*10^(-4)*(pi/180)^2, 0;0,10^(-4)];
Q=10^(-6)*eye(4);
Q(5,5)=10^(-3);
Q(6,6)=10^(-3);
nx=6; %dimenze stavu
kappa=0.0; %doporuceni ze skript


x_odh = [20,50,0,-12,0,0]';
P_odh = eye(6);
P_odh(6,6)=10^(-6);
P_odh(5,5)=10^(-6);

%vahy
w = zeros(13,1);
w(:) =  1/(2*(nx+kappa)); %vsechny vahy
w(1) = kappa/(nx+kappa);  % krome te prvni


for k = 1:length(z)
    %FILTRACE
    %urceni chi bodu 
    S = chol(P_odh)';
    chi = zeros(6,13);
    chi(:,1) = x_odh;
    for i=2:7
        chi(:,i) =  x_odh + sqrt(nx)*S(:,i-1); 
    end
    for i=8:13
        chi(:,i) =  x_odh - sqrt(nx)*S(:,i-7); 
    end

    %transformace chi bodu
    zeta = zeros(2,13);
    for i = 1:13
        zeta(:,i) = transformace_h(chi(:,i))';
    end

    %filtracni odhad mereni
    z_odh = 0;
    for i=1:13
        z_odh = z_odh + w(i)*zeta(:,i);
    end
    %filtracni  variance vystupu
    P_z = zeros(2);
    for i=1:13
        P_z = P_z + w(i)*(zeta(:,i)-z_odh)*(zeta(:,i)-z_odh)';
    end
    P_z = P_z+R;

    %filtracni  variance vstupXvystup
    P_xz = zeros(6,2);
    for i=1:13
        P_xz = P_xz + w(i)*(chi(:,i)-x_odh)*(zeta(:,i)-z_odh)';
    end

    %k. zisk
    K = P_xz*inv(P_z);

    %vypoceme filtrovane x a P
    x_filt(:,k) = x_odh + K*(z(:,k) - z_odh);
    P_filt(:,:,k) = P_odh - K*P_z*K';

    %PREDIKCE
    %urceni chi bodu 
    chi = zeros(6,13);
    chi(:,1) =x_filt(:,k);
    S = chol(P_filt(:,:,k))';
    for i=2:7
        chi(:,i) =  x_filt(:,k) + sqrt(nx)*S(:,i-1); 
    end
    for i=8:13
        chi(:,i) =  x_filt(:,k) - sqrt(nx)*S(:,i-7); 
    end


    %transformace chi bodu
    gamma = zeros(6,13);
    for i = 1:13
        gamma(:,i) = transformace_f(chi(:,i))';
    end

    %prediktivni odhad stavu
    x_odh = 0;
    for i=1:13
        x_odh = x_odh + w(i)*gamma(:,i);
    end

    %prediktivni variance
    P_odh = zeros(6);
    for i=1:13
            P_odh = P_odh + w(i)*(gamma(:,i)-x_odh)*(gamma(:,i)-x_odh)';
    end
    P_odh = P_odh+Q;
end
end








